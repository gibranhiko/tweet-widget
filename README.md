## Synopsis

Widget to consume the Twitter API: https://dev.twitter.com/

## Code Example

//Example calls

twitter.getUserTimeline({ screen_name: 'BoyCook', count: '10'}, error, success);

//Here we can get a set of 10 tweets from that user.

## Installation

1. Run the npm install command inside the root folder for downloading the server dependencies.
2. Run the npm install command inside grunt folder (public/grunt) for downloading the grunt dependencies 

//I'm working on setting all up in only one package.json to avoid double downloading

## API Reference
https://github.com/BoyCook/TwitterJSClient

